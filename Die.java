import java.util.Random;
public class Die
{
	private int faceValue;
	private Random number;
	
	
	public Die()
	{
	 this.faceValue=1;
	 
	}
		
	public int getFaceValue()
	{
		return this.faceValue;
	}
	
	public void roll()
	{
	Random number = new Random();
	this.faceValue= number.nextInt(6)+1;
	}
	
	public String toString()
	{
		return "faceValue " + this.faceValue;
	}
}