public class Board
{
	private Die die1;
	private Die die2;
	private boolean [] tiles;
	
	public Board()
	{
		this.die1=new Die();
		this.die2=new Die();
		this.tiles=new boolean[12];
	}
	
	public String toString()
	{
		String newTiles="";
		for(int i=0;i<tiles.length;i++)
		{
			if(!tiles[i])
			{
			newTiles+= (i+1)+" ";	
			}
			else
			newTiles+="X" + " ";
		}
		return newTiles;
	}
	
	public boolean playATurn()
	{
		die1.roll();
		die2.roll();
		System.out.println(die1);
		System.out.println(die2);
		
		
		int sumOfDice=die1.getFaceValue()+die2.getFaceValue();
		
		if (!tiles[sumOfDice-1])
		{
			System.out.println("Closing tile equal to sum: ");
			tiles[sumOfDice-1]=true;
			return false;
		}
		else if(!tiles[die1.getFaceValue()])
		{
			tiles[die1.getFaceValue()]=true;
			System.out.println("Closing tile with the same value as die one: ");
			return false;
		}
		
		else if(!tiles[die2.getFaceValue()])
		{
			tiles[die2.getFaceValue()]=true;
			System.out.println("Closing tile with the same value as die one: ");
			return false;
		}
		
		else
		{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
	
	
	
	
	
	
	
}